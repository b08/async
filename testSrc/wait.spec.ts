import { describe } from "@b08/test-runner";
import { wait } from "../src";
import { measure } from "./measure";

describe("wait", it => {
  it("returns waiting promise", async expect => {
    // arrange
    const time = 5;
    // act
    const lasted = await measure(() => wait(time));

    // assert
    expect.true(lasted >= time);
  });
});
