import { range } from "@b08/array";
import { describe } from "@b08/test-runner";
import { wait } from "../src";
import { runSequential } from "../src";
import { randomOf } from "./randomOf";

describe("runSequential", it => {
  it("runs sequential", async expect => {
    // arrange
    const input = range(5);

    // act
    const results = await runSequential(input, async (item) => {
      await wait(randomOf(5));
      return item;
    });

    // assert
    expect.deepEqual(results, input);
  });
});
