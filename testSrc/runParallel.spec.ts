import { range, areEquivalent } from "@b08/array";
import { describe } from "@b08/test-runner";
import { wait, runParallel } from "../src";
import { measure } from "./measure";

describe("runParallel", it => {
  it("runs in parallel", async expect => {
    // arrange
    const input = range(4);

    // act
    let result: number[];
    const lasted = await measure(async () => result = await runParallel(input, async (item) => {
      await wait(50);
      return item;
    }));


    // assert
    expect.true(lasted < 200);
    expect.true(areEquivalent(result, input));
  });
});
