import { waitFor } from "../src";
import { describe } from "@b08/test-runner";
import { measure } from "./measure";

describe("waitFor", it => {
  it("waits for signal", async expect => {
    // arrange
    const time = 5;
    const count = 5;
    // act
    let index = 0;
    const lasted = await measure(() => waitFor(() => ++index >= count, null, time));

    // assert
    expect.true(lasted >= time * count);
    expect.equal(index, count);
  });

  it("waits for timeout", async expect => {
    // arrange
    const time = 20;
    const count = 5;
    // act
    let index = 0;
    const lasted = await measure(() => waitFor(() => ++index >= count, 15, time));

    // assert
    expect.true(lasted + 1 < time * count);
    expect.true(index < count);
  });
});
