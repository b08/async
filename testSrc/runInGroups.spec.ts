import { range, areEquivalent } from "@b08/array";
import { describe } from "@b08/test-runner";
import { runInGroups, delay } from "../src";
import { measure } from "./measure";

describe("runInGroups", it => {
  it("runs sequential groups in parallel", async expect => {
    // arrange
    const input = range(16);

    // act
    let result: number[];
    const lasted = await measure(async () => result = await runInGroups(input, async (item) => {
      await delay(50);
      return item;
    }, 4));

    // assert
    expect.true(lasted < 400);
    expect.true(lasted > 200);
    expect.true(areEquivalent(result, input));
  });
});
