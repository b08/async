export type WaitCondition = boolean | Promise<boolean>;
export function waitFor(condition: () => WaitCondition): Promise<void>;
export function waitFor(condition: () => WaitCondition, timeout: number): Promise<boolean>;
export function waitFor(condition: () => WaitCondition, timeout: number, interval: number): Promise<boolean>;
export function waitFor(condition: () => WaitCondition, timeout?: number, interval: number = 1000): Promise<boolean | void> {
  return new Promise<boolean>(res => {
    let resolved = false;
    const handler = setInterval(async () => { if (await condition()) { resolve(true); } }, interval);
    if (timeout == null || timeout === 0) { return; }
    setTimeout(() => resolve(false), timeout);
    function resolve(result: boolean): void {
      if (resolved) { return; }
      resolved = true;
      clearInterval(handler);
      res(result);
    }
  });
}
