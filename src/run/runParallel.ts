import { RunAction } from "./runAction.type";

export function runParallel<TI, TR>(input: TI[], action: RunAction<TI, TR>): Promise<TR[]> {
  return Promise.all(input.map(action));
}


