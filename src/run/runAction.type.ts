export type RunAction<TI, TR> = (item: TI) => Promise<TR>;
