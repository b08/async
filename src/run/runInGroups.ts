import { splitInGroups, flatMap } from "@b08/array";
import { RunAction } from "./runAction.type";
import { runSequential } from "./runSequential";
import { runParallel } from "./runParallel";

export async function runInGroups<TI, TR>(input: TI[], action: RunAction<TI, TR>, groups: number = 4): Promise<TR[]> {
  const inputGroups = splitInGroups(input, groups);
  const results = await runParallel(inputGroups, group => runSequential(group, action));
  return flatMap(results);
}
